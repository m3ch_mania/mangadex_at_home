FROM eclipse-temurin:17-jre

WORKDIR /mangahome
ADD /build/libs/mangadex_at_home.jar /mangahome/mangadex_at_home.jar

EXPOSE 443 8080

STOPSIGNAL 2

CMD exec java \
    -Dfile-level=off \
    -Dstdout-level=info \
    -jar mangadex_at_home.jar
