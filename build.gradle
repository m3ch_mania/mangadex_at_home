plugins {
    id "jacoco"
    id "java"
    id "org.jetbrains.kotlin.jvm" version "1.6.0"
    id "org.jetbrains.kotlin.kapt" version "1.6.0"
    id "application"
    id "com.github.johnrengelman.shadow" version "7.0.0"
    id "com.diffplug.spotless" version "5.8.2"
    id "net.afanasev.sekret" version "0.1.1-RC3"
    id "com.palantir.git-version" version "0.12.3"
}

group = "com.mangadex"
version = System.getenv("VERSION") ?: gitVersion()

application {
    mainClass = "mdnet.MainKt"
}

repositories {
    mavenCentral()
}

configurations {
    runtime.exclude group: "com.sun.mail", module: "javax.mail"
}

dependencies {
    implementation "org.jetbrains.kotlin:kotlin-reflect"

    compileOnly group: "net.afanasev", name: "sekret-annotation", version: "0.1.1-RC3"

    implementation group: "commons-io", name: "commons-io", version: "2.11.0"
    implementation group: "org.apache.commons", name: "commons-compress", version: "1.21"
    implementation group: "ch.qos.logback", name: "logback-classic", version: "1.3.0-alpha4"

    implementation group: "io.micrometer", name: "micrometer-registry-prometheus", version: "1.8.3"
    implementation group: "com.maxmind.geoip2", name: "geoip2", version: "2.15.0"

    implementation platform(group: "org.http4k", name: "http4k-bom", version: "4.19.3.0")
    implementation platform(group: "com.fasterxml.jackson", name: "jackson-bom", version: "2.13.1")
    implementation platform(group: "io.netty", name: "netty-bom", version: "4.1.74.Final")

    implementation group: "org.http4k", name: "http4k-core"
    implementation group: "org.http4k", name: "http4k-resilience4j"
    implementation group: "io.github.resilience4j", name: "resilience4j-micrometer", version: "1.7.1"
    implementation group: "org.http4k", name: "http4k-format-jackson"
    implementation group: "com.fasterxml.jackson.dataformat", name: "jackson-dataformat-yaml"
    implementation group: "com.fasterxml.jackson.datatype", name: "jackson-datatype-jsr310"
    implementation group: "org.http4k", name: "http4k-client-okhttp"
    implementation group: "org.http4k", name: "http4k-metrics-micrometer"
    implementation group: "org.http4k", name: "http4k-server-netty"
    implementation group: "io.netty", name: "netty-codec-haproxy"
    implementation group: "io.netty", name: "netty-transport-native-epoll", classifier: "linux-x86_64"
    implementation group: "io.netty.incubator", name: "netty-incubator-transport-native-io_uring", version: "0.0.11.Final", classifier: "linux-x86_64"
    testImplementation group: "org.http4k", name: "http4k-testing-kotest"
    runtimeOnly group: "io.netty", name: "netty-tcnative-boringssl-static", version: "2.0.48.Final"

    implementation group: "com.zaxxer", name: "HikariCP", version: "4.0.3"
    implementation group: "org.xerial", name: "sqlite-jdbc", version: "3.34.0"
    implementation "org.ktorm:ktorm-core:$ktorm_version"
    implementation "org.ktorm:ktorm-jackson:$ktorm_version"

    implementation "info.picocli:picocli:$picocli_version"
    kapt "info.picocli:picocli-codegen:$picocli_version"

    testImplementation "io.kotest:kotest-runner-junit5:$kotest_version"
    testImplementation "io.kotest:kotest-assertions-core:$kotest_version"
    testImplementation "io.mockk:mockk:1.12.2"
}

tasks.withType(Test) {
    useJUnitPlatform()
    javaLauncher = javaToolchains.launcherFor {
        languageVersion = JavaLanguageVersion.of(8)
    }
}
tasks.register("testDev", Test) {
    group = "verification"
    useJUnitPlatform()
    filter {
        excludeTestsMatching '*SlowTest'
    }
}


kapt {
    arguments {
        arg("project", "${project.group}/${project.name}")
    }
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType(org.jetbrains.kotlin.gradle.tasks.KotlinCompile).all {
    kotlinOptions {
        freeCompilerArgs = ["-Xjsr305=strict"]
        jvmTarget = "1.8"
    }
}

spotless {
    kotlin {
        ktlint("0.40.0").userData(["disabled_rules": "no-wildcard-imports"])
        licenseHeaderFile "license_header"
        trimTrailingWhitespace()
        endWithNewline()
    }
}

tasks.register("generateVersion", Copy) {
    def templateContext = [version: version]
    inputs.properties templateContext
    from "src/template/java"
    into "$buildDir/generated/java"
    expand templateContext
}
compileJava.dependsOn generateVersion
sourceSets.main.java.srcDir generateVersion

// https://gist.github.com/medvedev/968119d7786966d9ed4442ae17aca279
tasks.register("depsize") {
    description = 'Prints dependencies for "default" configuration'
    doLast() {
        listConfigurationDependencies(configurations.default)
    }
}

tasks.register("depsizeAll") {
    description = 'Prints dependencies for all available configurations'
    doLast() {
        configurations
                .findAll { it.isCanBeResolved() }
                .each { listConfigurationDependencies(it) }
    }
}

def listConfigurationDependencies(Configuration configuration) {
    def formatStr = "%,10.2f"

    def size = configuration.collect { it.length() / (1024 * 1024) }.sum()

    def out = new StringBuffer()
    out << "\nConfiguration name: \"${configuration.name}\"\n"
    if (size) {
        out << 'Total dependencies size:'.padRight(65)
        out << "${String.format(formatStr, size)} Mb\n\n"

        configuration.sort { -it.length() }
                .each {
                    out << "${it.name}".padRight(65)
                    out << "${String.format(formatStr, (it.length() / 1024))} kb\n"
                }
    } else {
        out << 'No dependencies found'
    }
    println(out)
}
