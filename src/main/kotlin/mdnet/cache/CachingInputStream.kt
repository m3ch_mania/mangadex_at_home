/*
Mangadex@Home
Copyright (c) 2020, MangaDex Network
This file is part of MangaDex@Home.

MangaDex@Home is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MangaDex@Home is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this MangaDex@Home.  If not, see <http://www.gnu.org/licenses/>.
*/
package mdnet.cache

import org.apache.commons.io.IOUtils
import org.apache.commons.io.input.ProxyInputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.lang.Runnable
import java.util.concurrent.ExecutorService
import kotlin.Throws

class CachingInputStream(
    response: InputStream?,
    private val executor: ExecutorService,
    private val cache: OutputStream,
    private val onClose: Runnable
) : ProxyInputStream(response) {
    private var eofReached = false

    @Throws(IOException::class)
    override fun close() {
        if (eofReached) {
            try {
                `in`.close()
            } catch (ignored: IOException) {
            }
            try {
                cache.close()
            } catch (ignored: IOException) {
            }
            onClose.run()
        } else {
            executor.submit {
                try {
                    IOUtils.copy(`in`, cache)
                } catch (ignored: IOException) {
                } finally {
                    try {
                        `in`.close()
                    } catch (ignored: IOException) {
                    }
                    try {
                        cache.close()
                    } catch (ignored: IOException) {
                    }
                    onClose.run()
                }
            }
        }
    }

    @Throws(IOException::class)
    override fun read(): Int {
        val ch = super.read()
        if (ch != IOUtils.EOF) {
            try {
                cache.write(ch)
            } catch (ignored: IOException) {
                // don't let write failures affect the image loading
            }
        } else {
            eofReached = true
        }
        return ch
    }

    @Throws(IOException::class)
    override fun read(bts: ByteArray, st: Int, end: Int): Int {
        val n = super.read(bts, st, end)
        if (n != IOUtils.EOF) {
            try {
                cache.write(bts, st, n)
            } catch (ignored: IOException) {
                // don't let write failures affect the image loading
            }
        } else {
            eofReached = true
        }
        return n
    }

    @Throws(IOException::class)
    override fun read(bts: ByteArray): Int {
        val n = super.read(bts)
        if (n != IOUtils.EOF) {
            try {
                cache.write(bts, 0, n)
            } catch (ignored: IOException) {
                // don't let write failures affect the image loading
            }
        } else {
            eofReached = true
        }
        return n
    }
}
