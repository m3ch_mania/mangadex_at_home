/*
Mangadex@Home
Copyright (c) 2020, MangaDex Network
This file is part of MangaDex@Home.

MangaDex@Home is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MangaDex@Home is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this MangaDex@Home.  If not, see <http://www.gnu.org/licenses/>.
*/
package mdnet.cache

import org.ktorm.schema.*
import org.ktorm.schema.Table

object DbImage : Table<Nothing>("IMAGES") {
    val id = varchar("id").primaryKey()
    val accessed = timestamp("accessed")
    val size = int("size")
}

const val INIT_TABLE = """
create table if not exists Images(
    id varchar primary key not null,
    size integer not null,
    accessed timestamp not null default CURRENT_TIMESTAMP,
    disk_size integer as ((size + 4095) / 4096 * 4096)
);
drop index if exists Images_lastAccessed_idx;
create index if not exists Images_accessed on Images(accessed);
"""

const val SIZE_TAKEN_SQL = "select sum(disk_size) from Images"

const val IMAGES_TO_PRUNE = """
select id, disk_size from Images order by accessed asc limit 1000
"""
