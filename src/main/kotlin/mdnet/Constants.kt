/*
Mangadex@Home
Copyright (c) 2020, MangaDex Network
This file is part of MangaDex@Home.

MangaDex@Home is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MangaDex@Home is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this MangaDex@Home.  If not, see <http://www.gnu.org/licenses/>.
*/
package mdnet

import java.time.Duration

object Constants {
    const val CLIENT_BUILD = 31

    @JvmField val MAX_AGE_CACHE: Duration = Duration.ofDays(14)

    const val MAX_READ_TIME_SECONDS = 300
    const val MAX_WRITE_TIME_SECONDS = 60

    // General list of ports to which Firefox and Chromium will not send HTTP requests for security reasons
    // See:
    //     * https://chromium.googlesource.com/chromium/src.git/+/refs/heads/master/net/base/port_util.cc
    //     * https://developer.mozilla.org/en-US/docs/Mozilla/Mozilla_Port_Blocking#Blocked_Ports
    @JvmField val RESTRICTED_PORTS = intArrayOf(
        1, // tcpmux
        7, // echo
        9, // discard
        11, // systat
        13, // daytime
        15, // netstat
        17, // qotd
        19, // chargen
        20, // ftp data
        21, // ftp access
        22, // ssh
        23, // telnet
        25, // smtp
        37, // time
        42, // name
        43, // nicname
        53, // domain
        77, // priv-rjs
        79, // finger
        87, // ttylink
        95, // supdup
        101, // hostriame
        102, // iso-tsap
        103, // gppitnp
        104, // acr-nema
        109, // pop2
        110, // pop3
        111, // sunrpc
        113, // auth
        115, // sftp
        117, // uucp-path
        119, // nntp
        123, // NTP
        135, // loc-srv /epmap
        139, // netbios
        143, // imap2
        179, // BGP
        389, // ldap
        427, // SLP (Also used by Apple Filing Protocol)
        465, // smtp+ssl
        512, // print / exec
        513, // login
        514, // shell
        515, // printer
        526, // tempo
        530, // courier
        531, // chat
        532, // netnews
        540, // uucp
        548, // AFP (Apple Filing Protocol)
        556, // remotefs
        563, // nntp+ssl
        587, // smtp (rfc6409)
        601, // syslog-conn (rfc3195)
        636, // ldap+ssl
        993, // ldap+ssl
        995, // pop3+ssl
        2049, // nfs
        3659, // apple-sasl / PasswordServer
        4045, // lockd
        6000, // X11
        6665, // Alternate IRC [Apple addition]
        6666, // Alternate IRC [Apple addition]
        6667, // Standard IRC [Apple addition]
        6668, // Alternate IRC [Apple addition]
        6669, // Alternate IRC [Apple addition]
        6697 // IRC + TLS
    )
}
